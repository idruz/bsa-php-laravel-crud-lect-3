<?php

namespace App\Repository;

use App\Models\{Buyer, Order, OrderItem, Product};
use Illuminate\Database\Eloquent\Collection;

class OrderRepository implements OrderRepositoryInterface
{
    public function getAll(): Collection
    {
        return Order::all();
    }

    public function getById(int $orderId)
    {
        return Order::findOrFail($orderId);
    }

    public function deleteOrder(int $orderId)
    {
        Order::destroy($orderId);
    }

    public function createOrder(array $orderDetails)
    {
        /**
         * TODO: refactor
         * Не совсем правильно здесь выводить 404
         */
        $buyer = Buyer::findOrFail($orderDetails['buyerId']);

        $modelOrder = new Order();
        $modelOrder->buyer_id = $buyer->id;
        /**
         * Лучше использовать транзакции
         */
        if ($modelOrder->save()) {
            foreach ($orderDetails['orderItems'] as $item) {
                $productModel = Product::find($item['productId']);
                if (!empty($productModel)) {
                    $discount = null;
                    $sum = intval($item['productQty']) * $productModel->price;
                    if (!empty($item['productDiscount'])) {
                        $discount = intval(trim($item['productDiscount'], '%'));
                        $sum = ($productModel->price - ($productModel->price * ($discount/100))) * intval($item['productQty']);
                    }

                    $orderItemModel = new OrderItem();
                    $orderItemModel->order_id = $modelOrder->id;
                    $orderItemModel->product_id = $productModel->id;
                    $orderItemModel->quantity = $item['productQty'];
                    $orderItemModel->price = $productModel->price;
                    $orderItemModel->discount = $discount;
                    $orderItemModel->sum = $sum;

                    $orderItemModel->save();
                }
            }
            return true;
        }
        return false;
    }

    public function updateOrder(int $orderId, array $orderDetails)
    {
        /**
         * TODO: refactor
         * Насильно передадим $orderId
         * в массив
         * так как могут передать в урл один айди
         * а в параметрах другой
         */
        $orderDetails['orderId'] = $orderId;

        /**
         * Не совсем правильно здесь выводить 404
         */
        $order = Order::findOrFail($orderId);

        foreach ($orderDetails['orderItems'] as $item) {
            $productModel = Product::find($item['productId']);
            $orderItemCollection = OrderItem::where([
                'order_id' => $order->id,
                'product_id' => intval($item['productId'])
            ])->get();

            if (
                !empty($productModel)
                and !empty($orderItemCollection)
            ) {

                $discount = null;
                $sum = intval($item['productQty']) * $productModel->price;
                if (!empty($item['productDiscount'])) {
                    $discount = intval(trim($item['productDiscount'], '%'));
                    $sum = ($productModel->price - ($productModel->price * ($discount/100))) * intval($item['productQty']);
                }

                foreach ($orderItemCollection as $orderItemModel) {
                    $orderItemModel->order_id = $order->id;
                    $orderItemModel->product_id = $productModel->id;
                    $orderItemModel->quantity = $item['productQty'];
                    $orderItemModel->price = $productModel->price;
                    $orderItemModel->discount = $discount;
                    $orderItemModel->sum = $sum;
                    $orderItemModel->update();
                }
            }
        }

        return true;
    }

}
