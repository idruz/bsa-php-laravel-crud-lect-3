<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Collection;

interface OrderRepositoryInterface
{
    public function getAll(): Collection;
    public function getById(int $orderId);
    public function deleteOrder(int $orderId);
    public function createOrder(array $orderDetails);
    public function updateOrder(int $orderId, array $orderDetails);
}
