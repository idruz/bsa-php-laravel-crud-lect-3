<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    use HasFactory;

    const AVAILABLE = 1;
    const NOT_AVAILABLE = 0;

    protected $table = 'my_products';
    protected $primaryKey = 'id';
    protected $attributes = [
        'available' => false
    ];

    protected $fillable = ['name', 'price', 'seller_id'];
    protected $guarded = ['available'];

    public function seller()
    {
        return $this->belongsTo(Seller::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'product_id');
    }

}
