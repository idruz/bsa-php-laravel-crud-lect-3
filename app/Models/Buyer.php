<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Buyer extends Model
{
    use HasFactory;

    protected $table = 'buyers';

    protected $fillable = [
        'name',
        'surname',
        'country',
        'city',
        'addressLine',
        'phone',
    ];

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class, 'order_id');
    }
}
