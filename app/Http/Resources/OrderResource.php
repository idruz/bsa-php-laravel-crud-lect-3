<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $items = [];
        $orderSum = 0;
        foreach ($this->resource->items as $item) {
            $items[] = [
                'productName' => $item->product->name,
                'productQty' => $item->quantity,
                'productPrice' => $item->price,
                'productDiscount' => (!empty($item->discount))
                    ? $item->discount . '%'
                    : 0,
                'productSum' => $item->sum
            ];

            $orderSum += $item->sum;
        }

        $data = [
            'orderId' => $this->id,
            'orderDate' => $this->created_at,
            'orderSum' => $orderSum,
            'orderItems' => $items,
            'buyer' => [
                'buyerFullName'=> $this->resource->buyer->name
                    . ' ' . $this->resource->buyer->surname,
                'buyerAddress'=> $this->resource->buyer->country
                    . ' ' . $this->resource->buyer->city
                    . ' ' . $this->resource->buyer->addressLine,
                'buyerPhone'=> $this->resource->buyer->phone
            ],
        ];

        return $data;
    }
}
