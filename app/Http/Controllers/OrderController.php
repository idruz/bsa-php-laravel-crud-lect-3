<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Repository\OrderRepositoryInterface;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    private OrderRepositoryInterface $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $orders = $this->orderRepository->getAll();

        return response()->json([
            'data' => $orders->map(function ($order){
                return new OrderResource($order);
            })
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $orderDetails = $request->only([
            'buyerId',
            'orderItems'
        ]);

        if (empty($orderDetails)) {
            return response()->json(
                null,
                JsonResponse::HTTP_NON_AUTHORITATIVE_INFORMATION
            );
        }

        return response()->json([
            'data' => $this->orderRepository->createOrder($orderDetails)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $order = $this->orderRepository->getById($id);

        return response()->json([
            'data' => new OrderResource($order)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $orderDetails = $request->only([
            'orderId',
            'orderItems'
        ]);

        if (empty($orderDetails)) {
            return response()->json(
                null,
                JsonResponse::HTTP_NON_AUTHORITATIVE_INFORMATION
            );
        }

        return response()->json([
            'data' => $this->orderRepository->updateOrder($id, $orderDetails)
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $this->orderRepository->deleteOrder($id);

        return response()->json(
            null,
            JsonResponse::HTTP_NO_CONTENT
        );
    }
}
