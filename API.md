# API Methods example json data
## Create method POST

```
POST

{
    "buyerId": "1",
    "orderItems": [
        {
            "productId": "1",
            "productQty": "1",
            "productDiscount": "79%"
        },
        {
            "productId": "2",
            "productQty": "3",
            "productDiscount": "19%"
        }
    ]
}
```

## UPDATE method PUT

```
PUT
{
    "orderId": "44",
    "orderItems": [
        {
            "productId": "1",
            "productQty": "13",
            "productDiscount": "21%"
        },
        {
            "productId": "2",
            "productQty": "11",
            "productDiscount": "23%"
        }
    ]
}
```

### Please! Change data to valid on your local machine!
