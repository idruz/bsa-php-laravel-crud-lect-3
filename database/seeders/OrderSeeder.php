<?php

namespace Database\Seeders;

use App\Models\{Buyer, Order};
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $buyers = Buyer::all()->toArray();
        if (!empty($buyers)) {
            foreach ($buyers as $buyer) {
                Order::factory(rand(1,5))->create([
                    'buyer_id' => $buyer['id']
                ]);
            }
        }
    }
}
