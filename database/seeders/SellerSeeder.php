<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Factories\{ProductFactory, SellerFactory};

class SellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new SellerFactory(10))->create()
            ->each(function ($seller) {
                $seller->products()->saveMany(
                    (new ProductFactory(20))->make(['seller_id'=>null])
                );
            });
    }
}
