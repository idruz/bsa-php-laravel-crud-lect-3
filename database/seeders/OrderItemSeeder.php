<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{Order, OrderItem, Product};

class OrderItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = Order::all()->toArray();
        $products = Product::where('available', Product::AVAILABLE)
            ->get()
            ->toArray();

        $randomProducts = [];
        if (!empty($products)) {
            $randomKeys = array_rand($products, rand(2,10));
            if (!empty($randomKeys)) {
                foreach ($randomKeys as $productId) {
                    $randomProducts[] = $products[$productId];
                }
            }
        }

        if (
            !empty($orders)
            and !empty($products)
            and !empty($randomProducts)
        ) {
            foreach ($orders as $order) {
                foreach ($randomProducts as $product) {
                    $quantity = rand(1,10);
                    $discount = rand(0, 100);

                    if (!empty($discount)) {
                        $sum = ($product['price'] - ($product['price'] * ($discount/100))) * $quantity;
                    } else {
                        $discount = null;
                        $sum = $quantity * $product['price'];
                    }

                    OrderItem::factory(1)->create([
                        'order_id' => $order['id'],
                        'product_id' => $product['id'],
                        'quantity' => $quantity,
                        'price' => $product['price'],
                        'discount' => $discount,
                        'sum' => $sum,
                    ]);
                }
            }
        }
    }
}
